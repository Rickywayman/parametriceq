//
//  BandpassFilter.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#ifndef __JuceBasicAudio__BandpassFilter__
#define __JuceBasicAudio__BandpassFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

#include "DelayLineFilter.h"
#include "SecondDelayLineFilter.h"

#include "ForwardDelayLineFilter.h"
#include "SecondForwardDelayLineFilter.h"

/**
 Bandpass Filters ARE FUN BUT ANNOYING
 */
class BandpassFilter :  public DelayLineFilter,
                        public SecondDelayLineFilter,
                        public ForwardDelayLineFilter,
                        public SecondForwardDelayLineFilter

{
public:
    //==============================================================================
    /**
     Constructor
     */
    BandpassFilter();
    
    /**
     Destructor
     */
    ~BandpassFilter();
    //==============================================================================
    /**
     Bandpass Filters are fun
     */
    float b1 (float input) override;
    float b2 (float input) override;
    float a1 (float feedforwardInput1) override;
    float a2 (float feedforwardInput2) override;
    
    float sigma();
    
    void setBandpassGain(float val);
    
    //void setFeedbackGain(float val);
    
    //friend void lowpassGain(LowpassFilter &cLowpassFilter);
    
private:
    //float feedbackGain;
    
    float outputa0;
    float outputa1;
    float outputa2;
    float outputb1;
    float outputb2;
    
    
    
    
};


#endif /* defined(__JuceBasicAudio__BandpassFilter__) */
