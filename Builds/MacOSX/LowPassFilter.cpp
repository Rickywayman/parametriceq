//
//  LowPassFilter.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#include "LowPassFilter.h"


LowpassFilter::LowpassFilter()
{
    setDelayInSamples1(1.f);
    
    
}

LowpassFilter::~LowpassFilter()
{
    
}

//==============================================================================

float LowpassFilter::b1(float input)
{
  
    float delayLineOutput1 = delayLineRead1();
    
    float output = (a0Gain * input) - ((1.f-feedbackGain1) * delayLineOutput1);
    //DBG("a0 = " << a0Gain);
    //DBG("b1 = " << feedbackGain1);
    
    delayLineWrite1(output);
    
    return output;
}
