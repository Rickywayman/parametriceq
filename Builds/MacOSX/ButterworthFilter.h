//
//  ButterworthFilter.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#ifndef __JuceBasicWindow__ButterworthFilter__
#define __JuceBasicWindow__ButterworthFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "DelayLineFilter.h"
#include "ForwardDelayLineFilter.h"

/**
 Class that runs a simple one-pole lowpass filter
 */
class ButterworthFilter :   public DelayLineFilter,
                            public ForwardDelayLineFilter
{
public:
    //==============================================================================
    /**
     Constructor
     */
    ButterworthFilter();
    
    /**
     Destructor
     */
    ~ButterworthFilter();
    //==============================================================================
    /**
     Function that executes a simple one-pole lowpass filter
     */
    float filter1(float input);
    float forwardfilter(float forwardinput);
    
private:
    
};

#endif /* defined(__JuceBasicWindow__ButterworthFilter__) */
