//
//  ButterworthFilter.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#include "ButterworthFilter.h"


ButterworthFilter::ButterworthFilter()
{
    setDelayInSamples1(1.f);
    
    
}

ButterworthFilter::~ButterworthFilter()
{
    
}



//==============================================================================

//float ButterworthFilter::filter(float input)
//{
    
  // }

//     yv[0] =   (ax[0] * xv[0] + ax[1] * xv[1] + ax[2] * xv[2]

float ButterworthFilter::filter1(float input)
{
    
    float delayLineOutput1 = delayLineRead1();
    
    
    float output = (feedbackGain1 * input) + ((1.f-feedbackGain1) * delayLineOutput1);
    
    //float output = (feedbackGain * forwardinput) + ((1.f-feedbackGain) * feedforward);
    
    delayLineWrite1(output);
    
    return output;

    
}

float ButterworthFilter::forwardfilter(float forwardinput)
{
    
    //float delayLineOutput = delayLineRead();
    float feedforward = forwarddelayLineRead1();
    
    //float output = (feedbackGain * forwardinput) + ((1.f-feedbackGain) * delayLineOutput);
    
    float output = (1.f-feedbackGain1) * feedforward;
    
    //delayLineWrite(output);
    forwarddelayLineWrite1(output);
    
    return output;
    
    
}