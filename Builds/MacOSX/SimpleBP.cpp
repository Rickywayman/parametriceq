
//
//  SimpleBP.cpp
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#include "SimpleBP.h"

/** */
SimpleBP::SimpleBP()
{
    
//    setDelayInSamples1(1.1);
//    setDelayInSamples2(2.f);
//    
//    setForwardDelayInSamples1(1.f);
//    setForwardDelayInSamples2(2.f);
    // I dont think I have cracked the art of filter orders yet. as you can see from my fuzz measure analysis, 1 and two sample delays cause peaks around 20khz
    
    
    a0 = a1 = a2 = b1 = b2 = 0;
    x1 = x2 = y1 = y2 = 0;
    
    config(1000, 44100, 10);
}

/** */
SimpleBP::~SimpleBP()
{
    
}


void SimpleBP::config(double fc, double fs, double Q)
{
    float theta;
    float beta;
    float gamma;
    
//    DBG("fc = " << fc);
//    DBG("q = " << Q);
    
    theta = ( 2.0 * M_PI * fc ) / fs;
    beta = ( ( 1 - tan ( theta / ( 2 * Q ) ) ) / ( 1 + tan ( theta / ( 2 * Q ) ) ) ) * 0.5;
    gamma = ( 0.5 + beta ) * cos ( theta );
    a0 = 0.5 - beta;
    a1 = 0.0;
    a2 = -1 * ( 0.5 - beta );
    b1 = -2 * gamma;
    b2 = 2 * beta;
}


double SimpleBP::process(double x0)
{
    double y0 = a0 * x0 + a1 * x1 + a2 * x2 - b1 * y1 - b2 * y2;
    x2 = x1;
    x1 = x0;
    y2 = y1;
    y1 = y0;
    
    return y0;
}


//clamping??? PI/2  ???


void SimpleBP::centerFrequency(double val)
{
    cf = val;
    DBG("center frequency = " << cf);
    
}

void SimpleBP::quality(double val)
{
    
    q = val;
    DBG("Q = " << q);
    
}










//==============================================================================

/** */
//float SimpleBP::b1(float input)
//{
//    float delayLineOutput1 = delayLineRead1();
//    
//    outputb1 = (feedbackGain1 * input) + (1.f-feedbackGain1) * delayLineOutput1;
//    
//    delayLineWrite1(outputb1);
//    //
//    return outputb1;
//}



