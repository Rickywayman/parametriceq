//
//  SimpleBP.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 27/01/2016.
//
//

#ifndef __JuceBasicAudio__SimpleBP__
#define __JuceBasicAudio__SimpleBP__


#include "../JuceLibraryCode/JuceHeader.h"



/**
 Bandpass Filters ARE FUN BUT ANNOYING
 */
class SimpleBP

{
public:
    //==============================================================================
    /**
     Constructor
     */
    SimpleBP();
    
    /**
     Destructor
     */
    ~SimpleBP();
    //==============================================================================
    /**
     Bandpass Filters are fun
     */
    void config(double fc, double fs, double q);
    double process(double x0);
    
    void centerFrequency(double val);
    void quality(double val);
    
private:
    double a0, a1, a2, b1 ,b2;
    double x1, x2, y1, y2;
    double cf;
    double q;
    
};





#endif /* defined(__JuceBasicAudio__SimpleBP__) */
