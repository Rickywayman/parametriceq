//
//  BandpassFilter.cpp
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#include "BandpassFilter.h"

/** */
BandpassFilter::BandpassFilter()
{
    
    setDelayInSamples1(1.1);
    setDelayInSamples2(2.f);
    
    setForwardDelayInSamples1(1.f);
    setForwardDelayInSamples2(2.f);
    // I dont think I have cracked the art of filter orders yet. as you can see from my fuzz measure analysis, 1 and two sample delays cause peaks around 20khz
    
}

/** */
BandpassFilter::~BandpassFilter()
{
    
}

//==============================================================================

/** */
float BandpassFilter::b1(float input)
{
    float delayLineOutput1 = delayLineRead1();
    
    outputb1 = (feedbackGain1 * input) + (1.f-feedbackGain1) * delayLineOutput1;
    
    delayLineWrite1(outputb1);
    //
    return outputb1;
}

/** */
float BandpassFilter::b2(float input)
{
    float delayLineOutput2 = delayLineRead2();
    
    outputb2 = (feedbackGain2 * input) + (1.f-feedbackGain2) * delayLineOutput2;
    
    delayLineWrite2(outputb2);
    //
    return outputb2;
}

/** */
float BandpassFilter::a1(float forwardinput1)
{
    float feedforward1 = forwarddelayLineRead1();
    
    outputa1 = (forwardfeedbackGain1 * forwardinput1) + (1.f-forwardfeedbackGain1) * feedforward1;
    //
    forwarddelayLineWrite1(outputa1);
    
    return outputa1;
}

/** */
float BandpassFilter::a2(float forwardinput2)
{
    float feedforward2 = forwarddelayLineRead2();

    outputa2 = (forwardfeedbackGain2 * forwardinput2) + (1.f-forwardfeedbackGain2) * feedforward2;
    //
    forwarddelayLineWrite2(outputa2);
    
    return outputa2;
}

//void BandpassFilter::setBandpassGain(float val)
//{
//    outputa0 = val;
//}
//
//float BandpassFilter::sigma()
//{
//    float sigma;
//    
//    sigma = outputa1 + outputa2 - outputb1 - outputb2;
//    
//    return sigma;
//}



