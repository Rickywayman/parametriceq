/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#define _USE_MATH_DEFINES

#include "Audio.h"

#include <math.h>
#include <complex>

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    //load the filePlayer into the audio source
    audioSourcePlayer.setSource (&filePlayer);
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
//    float theta;
//    float beta;
//    float gamma;
//    float a0, a1, a2, b1, b2;
//    float Q = 0.707; //butterworth principle
//    
//    theta = ( 2 * M_PI * 1000 ) / 44100;
//    beta = (( 1 - tanf(theta / 2 * Q) ) / ( 1 + tanf(theta / 2 * Q))) * 0.5;
//    gamma = ( 0.5 + beta ) * cos(theta);
//    a0 = 0.5 - beta;
//    a1 = 0.0;
//    a2 = -(0.5 - beta);
//    b1 = -2 * gamma;
//    b2 = 2 * beta;
//    
//    DBG("theta = " << theta);
//    DBG("beta = " << beta);
//    DBG("gamma = " << gamma);
//    DBG("a0 = " << a0);
//    DBG("a1 = " << a1);
//    DBG("a2 = " << a2);
//    DBG("b1 = " << b1);
//    DBG("b2 = " << b2);
//    DBG("Q = " << Q);
    
    
    double b1;
    double a0;
    double theta;
    //float beta;
    double gamma;
    
    //float Q = 0.707; //butterworth principle
    double param;
    double param2;
    
    theta = ( 2 * double_Pi * 100 ) / 44100;
    //beta = (( 1 - tanf(theta / 2 * Q) ) / ( 1 + tanf(theta / 2 * Q))) * 0.5;
    gamma = 2 - cos(theta);
    param = gamma * gamma;
    param2 = (param - 1 - gamma);
    b1 = sqrt(param2);
    a0 = 1 + b1;
    
    
    //template<class T> complex<T> sqrt (const complex<T>& x);
    
    //a0 = (((1 + b1) - min) / (max - min));
    //b1 = (sqrtf(param) / 1);
    //(((sqrtf(param)) - min) / (max - min));
    //b = (b1 - min) / (max - min);
    
    // scaledValue =
//    DBG("theta = " << theta);
//    DBG("gamma = " << gamma);
//    DBG("a0 = " << a0);
//    DBG("b1 = " << b1);
    
    //could try tesing double instead of float power
    // could try testing 1000Hz instead of feedbackGain
    //paranthesis
    //NAN not a number... why does the book tell me to do an equation that produces imaginary numbers?!
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
}

void Audio::setLowpassGain(float val)
{
    //DBG ("Audio Lowpass Gain   " << val);
    lowpassGain = val;
}

void Audio::setHighpassGain(float val)
{
    //DBG ("Audio Highpass Gain   " << val);
    highpassGain = val;
}

void Audio::setBandpassQ(float val)
{
    bandpassQ = val;
}

void Audio::setBandpassGain(float val)
{
    bandpassGain = val;
}

void Audio::getComboBox(int val)
{
    //DBG("AudioComboBoxValue: " << val);
    comboBox = val;
    
}

void Audio::setMasterVolume(float val)
{
    masterVolume = val;
}



/////////// When to use int Audio::getComboBox() orrrrr void Audio::getComboBox(int val)////////////

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    // get the audio from our file player - player puts samples in the output buffer
    audioSourcePlayer.audioDeviceIOCallback (inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);
    
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    //float inSampL;
    //float inSampR;
    
    while(numSamples--)
    {
        //float output = *inL;
        float input = *inL ;
        //float input = filePlayer.getNextAudioBlock;

        
        //float output = inSampL;
        
            if (comboBox == 1) // Lowpass
            {
                output = lowpassFilter.b1 (output) * lowpassGain;
                
            }
            else if(comboBox == 2) // Highpass
            {
                output = ((highpassFilter.b1 (output)) + *inL) * highpassGain;
            }
            else if(comboBox == 3) // Butterworth
            {
                //inSampL = (butterworthFilter.filter(*outL) + butterworthFilter.forwardfilter(input)) * BWgain;
            }
            else if(comboBox == 4) // Bandpass
            {
                
//                output = (
//                           bandpassFilter.a1(input)
//                          + bandpassFilter.a2(input)
//                          - bandpassFilter.b1(output)
//                          - bandpassFilter.b2(output))
//                             * masterVolume;
                
                //output = bandpass.process(input);
                output = PEQ.process(input);
                
            }
        
      // it seems as tho the FIR and IIR filters are working exactly the same... I wonder whether they are reading input and output from *inL the same....
        // yeah, there is absolutely no difference between input and output
        
        //inSampR = *outL;
        
        *outL = output;//inSampL * 1.f;
        *outR = output;//inSampL * 1.f;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    audioSourcePlayer.audioDeviceAboutToStart (device);
}

void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
}